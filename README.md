# Pressure Setup Control

Contains two simple possibilities to control the HiBPM Vacuum Setup by communicating with the Siemens LOGO! PLC over Ethernet.<br>
Requires Python [snap7 library](https://snap7.sourceforge.net/logo.html/).<br>
The LOGO runs an internal program containing a PI controller.<br>

1) pressure_set.py
2) Pressure_Control_JSONRPC_Server.py

## Contents

- pressure_set.py: A simple command line program to set a pressure and show the progression in a plot window
- Pressure_Control_JSONRPC_Server.py: A [JSON-RPC](https://www.jsonrpc.org/) (remote procedure call) using a TCP server

- PressureController.py: Class containing the methods for the Server
- pid_intern.lsc: LOGO program (Need Siemens LOGO Soft Comfort for viewing/editing)
- send_jsonrpc_command.py: An exemplary function call of the RPC Server

## Hardware Configuration

The analog in/outputs of the components need to be connected as shown in the picture.<br>
All parts also require a 24V power supply.<br>
The LOGO needs to run `pid_intern.lsc` in active mode.<br>
If communication problems should arise check if the LOGO IP adress matches with the device and if [PC connection is established](https://forumautomation.com/t/how-to-connect-and-download-program-on-logo-plc/8971).

<img src="docs/vacuum_setup_hardware_config.png" width=50% height=50%>
<img src="docs/Vacuum_setup_pid_intern.png" width=50% height=50%>
<img src="docs/LOGO_connection.png" width=30% height=30%>

## How to run

The vacuum pump has to be started manually for both methods. <br>

1) Start `pressure_set.py`.<br>
It has to be quit forcefully terminated as it is a continuous loop

2) Start the Server with `Pressure_Control_JSONRPC_Server.py`<br>
Send commands via the command line or call methods from a script.

In both cases the LOGO maintains the last set of parameters in its internal memory as holds the pressure.

### Pressure log

The `pressure_set.py` version writes a continuous log csv to a file `<pressure>mbar.txt` into a folder `.\log`

```csv
time (s)                press (mbar)    valve_v (V)
1668070578.421466       3.981	        5.100
1668070578.9257061	3.890	        5.040
1668070579.430647	3.981	        5.230
1668070579.9348676	3.981	        5.240
1668070580.4389927	3.981	        5.300
1668070580.9432273	4.169	        5.230           
...                     ...       	...       
```

## JSON-RPC

### How to adress?

The JSON command are given as dictionaries.
The "params" block is optional if there are none to give.
In case no return is expected "id" is also optional.

```json
{"jsonrpc": "2.0", "method": "<method>", "params": {"param1": "<param1>", "param2": "<param2>",...}, "id": 0}
```

A return value follows a similar structure

```json
{"jsonrpc": "2.0", "result": {"param1": "<param1>", "param2": "<param2>",...}, "id": 0}
```

The command is either sent using for example [netcat](https://en.wikipedia.org/wiki/Netcat) or other console commands like `curl`

```bash
echo '{"jsonrpc": "2.0", "method": "set_pressure", "params": {"set_press_mbar": 100.0}}' | nc localhost 8001
```
```bash
curl 127.0.0.1:8001 -d "'{"jsonrpc": "2.0", "method": "set_pressure", "params": {"set_press_mbar": 100.0}}'"
```

Or via a block of code similar to the ```send_jsonrpc_command.py``` example.

### Methods

#### State

Request the current values of the system

```json
{"jsonrpc": "2.0", "method": "get_state", "id": 0}
```

This will return the current pressure (mbar) and voltage (V) supplied to the needle valve

```json
{"jsonrpc": "2.0", "result": {"valve_volt_volt": 2.4, "gauge_press_mbar": 105.2}, "id": 0}
```

#### Set pressure

Set a pressure value.

Required parameter `set_pressure_mbar` (float)

```json
{"jsonrpc": "2.0", "method": "set_pressure", "params": {"set_press_mbar": 100.0}}
```

#### Open/Close valve

Completely open or shut the needle valve

```json
{"jsonrpc": "2.0", "method": "open_valve"}
```
```json
{"jsonrpc": "2.0", "method": "close_valve"}
```


