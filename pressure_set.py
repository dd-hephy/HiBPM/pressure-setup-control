"""
Quickly set and hold a pressure in the Vacuum Setup + Live plot and log file
"""

import os
import logging
import snap7
import numpy as np

import time
import matplotlib.pyplot as plt


logger = logging.getLogger(__name__)

# Snap 7 configuration
plc = snap7.logo.Logo()
plc.connect("192.168.0.73", 0x0200, 0x0300)

# Specify value
set_press = input("Please enter a pressure in mbar:\n")

#Initialize
set_volt = round(((5.5 + np.log10(float(set_press)))*100),0)
print('Gauge voltage to achieve: {0} V'.format((set_volt/100)))
run_num = int(set_press) #For the log file name

def log(file_name, line_to_append):
    #Appends a line to the log file

    with open(file_name, "a") as file_object:
        for line in line_to_append:
            file_object.write(line)

def log_plot(run_num):

    # checking if log folder exists
    if not os.path.exists("log"):
        os.makedirs("log")

    #Log file header
    if os.path.exists('log/log{}.txt'.format(run_num)):
        os.remove('log/log{}.txt'.format(run_num))
    log('log/log{}.txt'.format(run_num),
        'time (s) \t press (mbar) \t valve_v (V)\n')
    
    #Set up plot window
    fig, ax = plt.subplots(2, 1)

    #Pressure log
    ax[0].axhline(float(set_press), color = 'r', linestyle = 'dashed')
    ax[0].grid()
    ax[0].set_ylabel('Current pressure [mbar]')

    #Voltage log
    ax[1].grid()
    ax[1].set_ylabel('Valve voltage [V]')
    ax[1].set_xlabel('time  [s]')

    #Infinite logging loop
    while True:
        act_volt, act_press = read_gauge()
        valve_volt = read_valve()

        #File log
        log_line = ['{:7.3f}\t{:7.3f}\t{:7.3f}'.format((time.time() - start_time), act_press, (valve_volt/100)), '\n']
        log('log/log{}.txt'.format(run_num), log_line)

        #Console output
        print(round((time.time() - start_time),1), round(act_press,1), round((valve_volt/100),1))

        #Live plot
        ax[0].scatter((time.time() - start_time), act_press, c="royalblue", marker='.') #plot pressure
        ax[1].scatter((time.time() - start_time), valve_volt/1000, c="orange", marker='.')  #plot valve voltage

        #Set log speed
        plt.pause(0.5)

def set_pressure(set_volt):
    vm_address_am2 = ("VW1120")
    plc.write(vm_address_am2, int(set_volt))

    print('{} mbar = {} V sent to PI Controller'.format(set_press, set_volt))

def read_gauge():
    vm_address_am1 = ("VW1118")
    gauge_volt_num = plc.read(vm_address_am1)

    # Transform to mbar
    gauge_press = 10**((gauge_volt_num/100) - 5.5)
    return gauge_volt_num, gauge_press

def read_valve():
    vm_address_am3 = ("VW1122")
    gauge_volt_num = plc.read(vm_address_am3)

    return gauge_volt_num

def open_valve(valve_voltage):
    vm_address_am3 = ("VW1122")
    plc.write(vm_address_am3, valve_voltage)

##############################
##########MAIN ACTION#########
##############################

if plc.get_connected():
    logger.info("connected")

    set_pressure(set_volt)

    start_time = time.time()
    plt.show()
    log_plot(run_num)

    #Manual valve opening/closing (Range: 0-1000 = 0-10 V)
    #open_valve(0)
    #open_valve(1000) 

else:
    logger.error("Conncetion failed")

plc.disconnect()
logger.info("Disconnected")
plc.destroy()
