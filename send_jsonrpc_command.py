"""Building block to send commands to the JSON RPC Server and receive results"""

import json
import socket

def send_json_dict(hostname, port, request):
        """Send a command to a JSON-RPC API and get back results
           The command is converted from a dict to a JSON string

        Args:
            hostname (string)
            port (int)
            request (dict): Dict with command to send
        """

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((hostname, port))

            sock.sendall(json.dumps(request).encode('utf-8'))
            state = sock.recv(4096).decode('utf-8')

        return state

"""Example commands"""

#Current state
#print(send_json_dict('localhost', 8001, {"jsonrpc": "2.0", "method": "state", "id": 0}))

#Set pressure
#send_json_dict('localhost', 8001,
#{"jsonrpc": "2.0", "method": "set_pressure",
#"params": {"set_pressure_mbar": 300.0}, "id": 0})

#Open valve
#send_json_dict('localhost', 8001, {"jsonrpc": "2.0", "method": "open_valve", "id": 0})