import jsonrpc
import logging
import socketserver
import time
from typing import Any, Dict

from PressureController import PressureController

__author__ = "Andreas Gsponer"
__license__ = "GNU General Public License v3.0"

"""
Class to implement a simple JSON-RPC TCP Server for handling the Pressure Control of the Alpha Setup

This server handles and JSON-RPC requests and returns responses (for example
the current state of the pressure controller)
This class interfaces with the PressureController class, where the actual logic
is implemented.

The code is mostly adapted from the Diode Measurement application, see
https://github.com/hephy-dd/diode-measurement/blob/main/diode_measurement/plugins/tcpserver.py

"""

logger = logging.getLogger(__name__)

class TCPHandler(socketserver.BaseRequestHandler):

    buffer_size: int = 1024

    def handle(self) -> None:
        self.data = self.request.recv(self.buffer_size).strip().decode("utf-8")
        logger.info("%s wrote: %s", self.client_address[0], self.data)
        # self.server.messageReady.emit(format(self.data))
        response = self.server.rpcHandler.handle(self.data)
        if response:
            data = response.json.encode("utf-8")
            logger.info("%s returned: %s", self.client_address[0], response.json)
            # self.server.messageReady.emit(format(response.json))
            self.request.sendall(data)

class TCPServer(socketserver.TCPServer):

    allow_reuse_address: bool = True

class RPCHandler:
    """
    This class handles all JSON-RPC commands which are sent to the server.
    It should mostly just be an interface to the PressureController class.
    """

    def __init__(self) -> None:

        self.PressureController = PressureController()

        #Adding methods
        self.dispatcher = jsonrpc.Dispatcher()
        self.dispatcher["state"] = self.get_state
        self.dispatcher["set_pressure"] = self.set_pressure
        self.dispatcher["open_valve"] = self.open_valve
        self.dispatcher["close_valve"] = self.close_valve
        self.manager = jsonrpc.JSONRPCResponseManager()

    def handle(self, request) -> Dict[str, Any]:
        print("Got JSON string as request: ", request)

        return self.manager.handle(request, self.dispatcher)

    def set_pressure(self, **kwargs):
        pressure_to_set = kwargs["set_pressure_mbar"]    
        print("Set pressure was called with {} mbar".format(pressure_to_set))   
        self.PressureController.set_pressure(pressure_to_set)

    def get_state(self):
        print("Get state was called")
        gauge = self.PressureController.read_gauge()
        valve = self.PressureController.read_valve()

        response = {
            "jsonrpc": "2.0",
            "result": {"valve_voltage_volt": valve, "gauge_pressure_mbar": gauge},
            "id": 0
        }

        return response

    def open_valve(self):
        print("Open valve was called")
        self.PressureController.open_valve()

    def close_valve(self):
        print("Close valve was called")
        self.PressureController.close_valve()

class Server:
    """
    This class provides a wrapper for the TCP JSON-RPC server,
    with utilits to configure and start the server.
    """

    def __init__(self, hostname: str, port: int) -> None:
        """Constructor

        Args:
            hostname (str): Hostname
            port (int): Port to listen on
        """
        self.hostname = hostname
        self.port = port

    def _setupServer(self, server):
        """Setup the TCP server by adding the RPC handler

        Args:
            server (TCPServer): TCPServer to configure
        """
        server.rpcHandler = RPCHandler()

    def runServer(self) -> None:
        """Start running the server"""

        logger.info("TCP started %s:%s", hostname, port)
        try:
            with TCPServer((self.hostname, self.port), TCPHandler) as server:

                self._setupServer(server)
                server.serve_forever()
        except Exception as exc:
            logger.exception(exc)
        finally:
            logger.info("TCP stopped %s:%s", hostname, port)
            time.sleep(0.50)

if __name__ == "__main__":
    """ Run JSON-RPC locally on localhost:8001 """
    
    hostname = "127.0.0.1"
    port = 8001

    server = Server(hostname, port)
    server.runServer()