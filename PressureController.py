"""Class containing the methods for Pressure_Control_JSONRPC_Server.py
"""

import snap7
import logging
import numpy as np

logger = logging.getLogger(__name__)

class PressureController:

    def __init__(self):
        #Connect to LOGO
        self.plc = snap7.logo.Logo()
        self.plc.connect("192.168.0.73", 0x0200, 0x0300)

        if self.plc.get_connected():
            logger.info("PLC connected")
        else:
            logger.error("PLC conncetion failed")

    def read_gauge(self):
        """Read value from Analog In 1 (Pressure Gauge) connected to LOGO AM1
        
        Returns:
            gauge_press_mbar (float): Current pressure measured by Gauge
        """

        vm_address_am1 = ("VW1118")
        gauge_volt_num = self.plc.read(vm_address_am1)
        gauge_press_mbar = 10**((gauge_volt_num/100) - 5.5)

        return gauge_press_mbar

    def read_valve(self):
        """Read value from Analog Out 1 (Needle valve) connected to LOGO AM3
        
        Returns:
            valve_volt_volt (float): Current voltage supplied to Needle valve in Volt
        """

        vm_address_am3 = ("VW1122")
        valve_volt_num = self.plc.read(vm_address_am3)
        valve_volt_volt = valve_volt_num * 100

        return valve_volt_volt

    def set_pressure(self, set_press_mbar):
        """Send value to LOGO AM2 connected to Internal PI Controller
        
        Args:
            set_press_mbar (float): Desired pressure in mbar
        """

        print('Pressure set to {} mbar'.format(set_press_mbar))
        set_volt = round(((5.5 + np.log10(float(set_press_mbar)))*100),0)
        vm_address_am2 = ("VW1120")
        self.plc.write(vm_address_am2, int(set_volt))

    def close_valve(self):
        """ Completely shut needle valve
            Write value 0 to LOGO AM3 connected to Analog Out 1
        """
        vm_address_am3 = ("VW1122")
        self.plc.write(vm_address_am3, 0)

    def open_valve(self):
        """ Completely open needle valve
            Write value 1000 to LOGO AM3 connected to Analog Out 1
        """
        vm_address_am3 = ("VW1122")
        self.plc.write(vm_address_am3, 1000)